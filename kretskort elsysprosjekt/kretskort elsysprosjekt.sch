EESchema Schematic File Version 4
LIBS:kretskort elsysprosjekt-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L kretskort-elsysprosjekt-rescue:RN2483-I_RM095-RN2483-I_RM095-kretskort-elsysprosjekt-rescue-kretskort-elsysprosjekt-rescue MODULE1
U 1 1 5C6405B6
P 2600 3350
F 0 "MODULE1" H 2600 4817 50  0000 C CNN
F 1 "RN2483-I_RM095" H 2600 4726 50  0000 C CNN
F 2 "RN2483-I_RM095:MICROCHIP_RN2483-I%2fRM095" H 2600 3350 50  0001 L BNN
F 3 "None" H 2600 3350 50  0001 L BNN
F 4 "RN2483-I/RM095" H 2600 3350 50  0001 L BNN "Field4"
F 5 "Unavailable" H 2600 3350 50  0001 L BNN "Field5"
F 6 "None" H 2600 3350 50  0001 L BNN "Field6"
F 7 "Microchip" H 2600 3350 50  0001 L BNN "Field7"
F 8 "Sub-GHz Dual Band Long Range Transceiver LoRa Module" H 2600 3350 50  0001 L BNN "Field8"
	1    2600 3350
	1    0    0    -1  
$EndComp
Wire Notes Line
	950  5250 2550 5250
Wire Notes Line
	2550 5250 2550 6700
Wire Notes Line
	2550 6700 950  6700
Wire Notes Line
	950  6700 950  5250
Text Notes 950  5200 0    50   ~ 0
Programming header
$Sheet
S 10200 2300 500  1650
U 5C669D40
F0 "connectorSheet" 50
F1 "connectorSheet.sch" 50
F2 "5V" I L 10200 2400 50 
F3 "GND" I L 10200 2500 50 
F4 "pH-sig" I L 10200 2700 50 
F5 "tempSig" I L 10200 2850 50 
F6 "turbSig" I L 10200 3000 50 
F7 "kondSig" I L 10200 3150 50 
F8 "3V3" I L 10200 2600 50 
F9 "pHSwitch" I L 10200 3300 50 
F10 "tempSwitch" I L 10200 3450 50 
F11 "turbSwitch" I L 10200 3550 50 
F12 "kondSwitch" I L 10200 3700 50 
$EndSheet
Wire Wire Line
	10200 2400 9800 2400
Wire Wire Line
	9550 3000 10200 3000
$Comp
L power:GND #PWR0102
U 1 1 5C66F1D9
P 1700 6300
F 0 "#PWR0102" H 1700 6050 50  0001 C CNN
F 1 "GND" H 1705 6127 50  0000 C CNN
F 2 "" H 1700 6300 50  0001 C CNN
F 3 "" H 1700 6300 50  0001 C CNN
	1    1700 6300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5C66F33C
P 5150 7200
F 0 "C4" H 5265 7246 50  0000 L CNN
F 1 "10uF" H 5265 7155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5188 7050 50  0001 C CNN
F 3 "~" H 5150 7200 50  0001 C CNN
	1    5150 7200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 5C66F37C
P 6400 7200
F 0 "C5" H 6515 7246 50  0000 L CNN
F 1 "100uF" H 6515 7155 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D6.3mm_P2.50mm" H 6438 7050 50  0001 C CNN
F 3 "~" H 6400 7200 50  0001 C CNN
	1    6400 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 7050 5500 7050
Wire Wire Line
	6100 7050 6400 7050
Wire Wire Line
	5800 7350 5800 7450
Wire Wire Line
	6400 7350 6400 7450
Wire Wire Line
	6400 7450 5800 7450
Connection ~ 5800 7450
Wire Wire Line
	5800 7450 5150 7450
Wire Wire Line
	5150 7450 5150 7350
$Comp
L power:GND #PWR0103
U 1 1 5C671A1E
P 5800 7500
F 0 "#PWR0103" H 5800 7250 50  0001 C CNN
F 1 "GND" H 5805 7327 50  0000 C CNN
F 2 "" H 5800 7500 50  0001 C CNN
F 3 "" H 5800 7500 50  0001 C CNN
	1    5800 7500
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0104
U 1 1 5C671A53
P 8800 5900
F 0 "#PWR0104" H 8800 5750 50  0001 C CNN
F 1 "+5V" H 8815 6073 50  0000 C CNN
F 2 "" H 8800 5900 50  0001 C CNN
F 3 "" H 8800 5900 50  0001 C CNN
	1    8800 5900
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0105
U 1 1 5C671A88
P 6400 7050
F 0 "#PWR0105" H 6400 6900 50  0001 C CNN
F 1 "+3.3V" H 6415 7223 50  0000 C CNN
F 2 "" H 6400 7050 50  0001 C CNN
F 3 "" H 6400 7050 50  0001 C CNN
	1    6400 7050
	1    0    0    -1  
$EndComp
Connection ~ 6400 7050
$Comp
L power:+3.3V #PWR0106
U 1 1 5C673565
P 9100 2600
F 0 "#PWR0106" H 9100 2450 50  0001 C CNN
F 1 "+3.3V" H 9115 2773 50  0000 C CNN
F 2 "" H 9100 2600 50  0001 C CNN
F 3 "" H 9100 2600 50  0001 C CNN
	1    9100 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 2600 10200 2600
$Comp
L Device:C C2
U 1 1 5C66B612
P 4500 3500
F 0 "C2" H 4615 3546 50  0000 L CNN
F 1 "0.1uF" H 4615 3455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4538 3350 50  0001 C CNN
F 3 "~" H 4500 3500 50  0001 C CNN
	1    4500 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5C66DE97
P 5000 3500
F 0 "C3" H 5115 3546 50  0000 L CNN
F 1 "0.1uF" H 5115 3455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5038 3350 50  0001 C CNN
F 3 "~" H 5000 3500 50  0001 C CNN
	1    5000 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 3650 4500 3750
Wire Wire Line
	5000 3350 5000 3300
Wire Wire Line
	5000 3650 5000 3750
Wire Wire Line
	5000 3750 4500 3750
Connection ~ 4500 3750
Wire Wire Line
	4500 3750 4500 3800
$Comp
L power:GND #PWR0107
U 1 1 5C673E54
P 4500 3800
F 0 "#PWR0107" H 4500 3550 50  0001 C CNN
F 1 "GND" H 4505 3627 50  0000 C CNN
F 2 "" H 4500 3800 50  0001 C CNN
F 3 "" H 4500 3800 50  0001 C CNN
	1    4500 3800
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0108
U 1 1 5C673FAC
P 5000 3300
F 0 "#PWR0108" H 5000 3150 50  0001 C CNN
F 1 "+3V3" H 5015 3473 50  0000 C CNN
F 2 "" H 5000 3300 50  0001 C CNN
F 3 "" H 5000 3300 50  0001 C CNN
	1    5000 3300
	1    0    0    -1  
$EndComp
Text Notes 4350 4000 1    50   ~ 0
Kondensatorene skal ligge\nnær vcc pinsene på radioen\nog mikrokontrolleren
$Comp
L Device:C C1
U 1 1 5C6791AF
P 4800 2100
F 0 "C1" H 4915 2146 50  0000 L CNN
F 1 "0.1uF" H 4915 2055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4838 1950 50  0001 C CNN
F 3 "~" H 4800 2100 50  0001 C CNN
	1    4800 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5C6792CE
P 4800 1450
F 0 "R1" H 4870 1496 50  0000 L CNN
F 1 "10K" H 4870 1405 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4730 1450 50  0001 C CNN
F 3 "~" H 4800 1450 50  0001 C CNN
	1    4800 1450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5C679823
P 4800 2450
F 0 "#PWR0109" H 4800 2200 50  0001 C CNN
F 1 "GND" H 4805 2277 50  0000 C CNN
F 2 "" H 4800 2450 50  0001 C CNN
F 3 "" H 4800 2450 50  0001 C CNN
	1    4800 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 2250 4800 2450
$Comp
L power:+3V3 #PWR0110
U 1 1 5C67A75E
P 4800 1100
F 0 "#PWR0110" H 4800 950 50  0001 C CNN
F 1 "+3V3" H 4815 1273 50  0000 C CNN
F 2 "" H 4800 1100 50  0001 C CNN
F 3 "" H 4800 1100 50  0001 C CNN
	1    4800 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 1100 4800 1300
Wire Wire Line
	4800 1600 4800 1800
Connection ~ 4800 1800
Wire Wire Line
	4800 1800 4800 1950
Text Label 3250 900  2    50   ~ 0
RFH
$Comp
L power:GND #PWR0111
U 1 1 5C680214
P 3200 1100
F 0 "#PWR0111" H 3200 850 50  0001 C CNN
F 1 "GND" H 3205 927 50  0000 C CNN
F 2 "" H 3200 1100 50  0001 C CNN
F 3 "" H 3200 1100 50  0001 C CNN
	1    3200 1100
	1    0    0    -1  
$EndComp
Text Notes 4050 650  2    50   ~ 0
Antenne
Wire Wire Line
	1800 2850 1700 2850
Wire Wire Line
	3400 2150 3550 2150
Wire Wire Line
	3400 2250 3550 2250
Wire Wire Line
	1800 2450 1600 2450
Text Label 1600 2450 2    50   ~ 0
RFH
$Comp
L power:GND #PWR03
U 1 1 5C68DE19
P 3550 4650
F 0 "#PWR03" H 3550 4400 50  0001 C CNN
F 1 "GND" H 3555 4477 50  0000 C CNN
F 2 "" H 3550 4650 50  0001 C CNN
F 3 "" H 3550 4650 50  0001 C CNN
	1    3550 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 4550 3550 4550
Wire Wire Line
	3550 4550 3550 4650
$Comp
L kretskort-elsysprosjekt-rescue:U.FL-R-SMT-1(10)-U.FL-R-SMT-1_10_-kretskort-elsysprosjekt-rescue-kretskort-elsysprosjekt-rescue-kretskort-elsysprosjekt-rescue J1
U 1 1 5C690890
P 3900 950
F 0 "J1" H 3985 979 50  0000 L CNN
F 1 "U.FL-R-SMT-1(10)" H 3985 888 50  0000 L CNN
F 2 "U.FL-R-SMT-1_10_:HRS_U.FL-R-SMT-1(10)" H 3900 950 50  0001 L BNN
F 3 "U.FL Series 6 Ghz 50 Ohm Ultra-small SMT Coaxial Cable Receptacle" H 3900 950 50  0001 L BNN
F 4 "Hirose" H 3900 950 50  0001 L BNN "Field4"
F 5 "None" H 3900 950 50  0001 L BNN "Field5"
F 6 "Unavailable" H 3900 950 50  0001 L BNN "Field6"
F 7 "None" H 3900 950 50  0001 L BNN "Field7"
F 8 "U.FL-R-SMT-1_10_" H 3900 950 50  0001 L BNN "Field8"
	1    3900 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 900  3800 950 
Wire Wire Line
	3250 900  3800 900 
Wire Wire Line
	3800 1100 3800 1050
Wire Wire Line
	3200 1100 3800 1100
Wire Wire Line
	1700 5400 2050 5400
$Comp
L power:+3V3 #PWR05
U 1 1 5C695C86
P 2050 5400
F 0 "#PWR05" H 2050 5250 50  0001 C CNN
F 1 "+3V3" V 2065 5528 50  0000 L CNN
F 2 "" H 2050 5400 50  0001 C CNN
F 3 "" H 2050 5400 50  0001 C CNN
	1    2050 5400
	0    1    1    0   
$EndComp
Wire Wire Line
	11050 5400 11050 5550
$Comp
L power:GND #PWR06
U 1 1 5C6986EB
P 11050 5550
F 0 "#PWR06" H 11050 5300 50  0001 C CNN
F 1 "GND" H 11055 5377 50  0000 C CNN
F 2 "" H 11050 5550 50  0001 C CNN
F 3 "" H 11050 5550 50  0001 C CNN
	1    11050 5550
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR01
U 1 1 5C6A34B3
P 9800 2400
F 0 "#PWR01" H 9800 2250 50  0001 C CNN
F 1 "+5V" H 9815 2573 50  0000 C CNN
F 2 "" H 9800 2400 50  0001 C CNN
F 3 "" H 9800 2400 50  0001 C CNN
	1    9800 2400
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J9
U 1 1 5C6B9AF2
P 3350 6700
F 0 "J9" H 3456 6878 50  0000 C CNN
F 1 "BattConnector" H 3456 6787 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 3350 6700 50  0001 C CNN
F 3 "~" H 3350 6700 50  0001 C CNN
	1    3350 6700
	-1   0    0    1   
$EndComp
Text Label 5150 900  2    50   ~ 0
RFL
$Comp
L power:GND #PWR0113
U 1 1 5C6C0E7E
P 5100 1100
F 0 "#PWR0113" H 5100 850 50  0001 C CNN
F 1 "GND" H 5105 927 50  0000 C CNN
F 2 "" H 5100 1100 50  0001 C CNN
F 3 "" H 5100 1100 50  0001 C CNN
	1    5100 1100
	1    0    0    -1  
$EndComp
Text Notes 5950 650  2    50   ~ 0
Antenne
$Comp
L kretskort-elsysprosjekt-rescue:U.FL-R-SMT-1(10)-U.FL-R-SMT-1_10_-kretskort-elsysprosjekt-rescue-kretskort-elsysprosjekt-rescue-kretskort-elsysprosjekt-rescue J8
U 1 1 5C6C0E8A
P 5800 950
F 0 "J8" H 5885 979 50  0000 L CNN
F 1 "U.FL-R-SMT-1(10)" H 5885 888 50  0000 L CNN
F 2 "U.FL-R-SMT-1_10_:HRS_U.FL-R-SMT-1(10)" H 5800 950 50  0001 L BNN
F 3 "U.FL Series 6 Ghz 50 Ohm Ultra-small SMT Coaxial Cable Receptacle" H 5800 950 50  0001 L BNN
F 4 "Hirose" H 5800 950 50  0001 L BNN "Field4"
F 5 "None" H 5800 950 50  0001 L BNN "Field5"
F 6 "Unavailable" H 5800 950 50  0001 L BNN "Field6"
F 7 "None" H 5800 950 50  0001 L BNN "Field7"
F 8 "U.FL-R-SMT-1_10_" H 5800 950 50  0001 L BNN "Field8"
	1    5800 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 900  5700 950 
Wire Wire Line
	5150 900  5700 900 
Wire Wire Line
	5700 1100 5700 1050
Wire Wire Line
	5100 1100 5700 1100
Text Label 1600 2550 2    50   ~ 0
RFL
Wire Wire Line
	1800 2550 1600 2550
$Comp
L Device:C C6
U 1 1 5C701615
P 2750 7300
F 0 "C6" H 2865 7346 50  0000 L CNN
F 1 "10uF" H 2865 7255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2788 7150 50  0001 C CNN
F 3 "~" H 2750 7300 50  0001 C CNN
	1    2750 7300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C7
U 1 1 5C70161C
P 4000 7300
F 0 "C7" H 4115 7346 50  0000 L CNN
F 1 "100uF" H 4115 7255 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D6.3mm_P2.50mm" H 4038 7150 50  0001 C CNN
F 3 "~" H 4000 7300 50  0001 C CNN
	1    4000 7300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 7150 3100 7150
Wire Wire Line
	3700 7150 4000 7150
Wire Wire Line
	3400 7450 3400 7550
Wire Wire Line
	4000 7450 4000 7550
Wire Wire Line
	4000 7550 3400 7550
Connection ~ 3400 7550
Wire Wire Line
	3400 7550 2750 7550
Wire Wire Line
	2750 7550 2750 7450
$Comp
L power:GND #PWR0122
U 1 1 5C70162C
P 3400 7550
F 0 "#PWR0122" H 3400 7300 50  0001 C CNN
F 1 "GND" H 3405 7377 50  0000 C CNN
F 2 "" H 3400 7550 50  0001 C CNN
F 3 "" H 3400 7550 50  0001 C CNN
	1    3400 7550
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0124
U 1 1 5C7334B7
P 5150 7050
F 0 "#PWR0124" H 5150 6900 50  0001 C CNN
F 1 "+5V" H 5165 7223 50  0000 C CNN
F 2 "" H 5150 7050 50  0001 C CNN
F 3 "" H 5150 7050 50  0001 C CNN
	1    5150 7050
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J10
U 1 1 5C739B34
P 8000 1100
F 0 "J10" H 8106 1278 50  0000 C CNN
F 1 "Serial monitor" H 8106 1187 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 8000 1100 50  0001 C CNN
F 3 "~" H 8000 1100 50  0001 C CNN
	1    8000 1100
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPDT SW1
U 1 1 5C76821F
P 9300 5900
F 0 "SW1" H 9300 6185 50  0000 C CNN
F 1 "SW_SPDT" H 9300 6094 50  0000 C CNN
F 2 "SPDT_Switch:SPDT_Switch" H 9300 5900 50  0001 C CNN
F 3 "" H 9300 5900 50  0001 C CNN
	1    9300 5900
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_B_Micro J2
U 1 1 5C6962C0
P 10700 5850
F 0 "J2" H 10755 6317 50  0000 C CNN
F 1 "USB_B_Micro" H 10755 6226 50  0000 C CNN
F 2 "Connector_USB:USB_Micro-B_Molex-105017-0001" H 10850 5800 50  0001 C CNN
F 3 "~" H 10850 5800 50  0001 C CNN
	1    10700 5850
	-1   0    0    1   
$EndComp
Wire Wire Line
	2650 6600 3150 6600
$Comp
L power:GND #PWR0123
U 1 1 5C77D32D
P 2650 6700
F 0 "#PWR0123" H 2650 6450 50  0001 C CNN
F 1 "GND" H 2655 6527 50  0000 C CNN
F 2 "" H 2650 6700 50  0001 C CNN
F 3 "" H 2650 6700 50  0001 C CNN
	1    2650 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 5900 8800 5900
Connection ~ 5150 7050
Text Label 4000 7150 0    50   ~ 0
Batt_5V
Text Label 9500 5800 0    50   ~ 0
Batt_5V
Wire Wire Line
	10700 5450 10700 5400
Wire Wire Line
	10700 5400 10800 5400
Wire Wire Line
	10800 5450 10800 5400
Connection ~ 10800 5400
Wire Wire Line
	10800 5400 11050 5400
Wire Wire Line
	10400 6050 9700 6050
Wire Wire Line
	9700 6050 9700 6000
Wire Wire Line
	9700 6000 9500 6000
Wire Wire Line
	7250 1800 7250 1850
Wire Wire Line
	8500 2700 8500 2450
Wire Wire Line
	8500 2450 8050 2450
Wire Wire Line
	8500 2700 10200 2700
Wire Wire Line
	8050 2550 8400 2550
Wire Wire Line
	8400 2550 8400 2850
Wire Wire Line
	8400 2850 10200 2850
Wire Wire Line
	8050 2950 9550 2950
Wire Wire Line
	9550 2950 9550 3000
Wire Wire Line
	8050 3150 10200 3150
Wire Wire Line
	8050 3300 8050 3350
Wire Wire Line
	8050 3300 10200 3300
Wire Wire Line
	8050 3450 10200 3450
Wire Wire Line
	8050 3550 10200 3550
Wire Wire Line
	8050 3700 8050 3650
Wire Wire Line
	8050 3700 10200 3700
Wire Wire Line
	5900 2450 6450 2450
Wire Wire Line
	6450 2550 5900 2550
$Comp
L kretskort-elsysprosjekt-rescue:FT230XS-Interface_USB-kretskort-elsysprosjekt-rescue U3
U 1 1 5C92293D
P 4850 5300
F 0 "U3" H 5250 6150 50  0000 C CNN
F 1 "FT230XS" H 5250 6000 50  0000 C CNN
F 2 "Package_SO:SSOP-16_3.9x4.9mm_P0.635mm" H 5300 4650 50  0001 C CNN
F 3 "http://www.ftdichip.com/Products/ICs/FT230X.html" H 4850 5300 50  0001 C CNN
	1    4850 5300
	1    0    0    -1  
$EndComp
Text Label 3050 5200 0    50   ~ 0
D-
Text Label 3050 5300 0    50   ~ 0
D+
$Comp
L Device:R R7
U 1 1 5C9300D4
P 4000 5300
F 0 "R7" V 4100 5300 50  0000 C CNN
F 1 "27" V 4200 5300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3930 5300 50  0001 C CNN
F 3 "~" H 4000 5300 50  0001 C CNN
	1    4000 5300
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 5C930D52
P 4000 5200
F 0 "R6" V 3793 5200 50  0000 C CNN
F 1 "27" V 3884 5200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3930 5200 50  0001 C CNN
F 3 "~" H 4000 5200 50  0001 C CNN
	1    4000 5200
	0    1    1    0   
$EndComp
$Comp
L Device:C C9
U 1 1 5C94D58C
P 3500 5550
F 0 "C9" H 3600 5550 50  0000 L CNN
F 1 "47p" H 3600 5450 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3538 5400 50  0001 C CNN
F 3 "~" H 3500 5550 50  0001 C CNN
	1    3500 5550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 5C94D61E
P 3200 5550
F 0 "C8" H 3050 5650 50  0000 L CNN
F 1 "47p" H 2950 5550 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3238 5400 50  0001 C CNN
F 3 "~" H 3200 5550 50  0001 C CNN
	1    3200 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 5200 3500 5200
Wire Wire Line
	3050 5300 3200 5300
Wire Wire Line
	3200 5300 3200 5400
Connection ~ 3200 5300
Wire Wire Line
	3500 5400 3500 5200
Connection ~ 3500 5200
Wire Wire Line
	3500 5700 3300 5700
$Comp
L power:GND #PWR0101
U 1 1 5C9614CF
P 3300 5750
F 0 "#PWR0101" H 3300 5500 50  0001 C CNN
F 1 "GND" H 3305 5577 50  0000 C CNN
F 2 "" H 3300 5750 50  0001 C CNN
F 3 "" H 3300 5750 50  0001 C CNN
	1    3300 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 5700 3300 5750
Connection ~ 3300 5700
Wire Wire Line
	3300 5700 3200 5700
Wire Wire Line
	2750 6700 3150 6700
$Comp
L Connector:AVR-PDI-6 J3
U 1 1 5C986214
P 1800 5800
F 0 "J3" H 1520 5846 50  0000 R CNN
F 1 "AVR-PDI-6" H 1520 5755 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" V 1550 5750 50  0001 C CNN
F 3 " ~" H 525 5250 50  0001 C CNN
	1    1800 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 6200 1700 6300
$Comp
L atmega4809:ATmega4809 U4
U 1 1 5C8BAC26
P 7250 3550
F 0 "U4" H 8050 5600 60  0000 C CNN
F 1 "ATmega4809" H 8100 5450 60  0000 C CNN
F 2 "ATMEGA4809-AFR:QFP50P900X900X120-48N" H 7250 3150 60  0001 C CNN
F 3 "" H 7250 3150 60  0001 C CNN
	1    7250 3550
	1    0    0    -1  
$EndComp
Text Label 8050 4650 0    50   ~ 0
UPDI
Wire Wire Line
	4800 1800 5650 1800
Wire Wire Line
	5650 1800 5650 2850
Wire Wire Line
	5650 2850 6450 2850
$Comp
L Device:R R8
U 1 1 5C9A3B22
P 9950 1050
F 0 "R8" H 10020 1096 50  0000 L CNN
F 1 "10K" H 10020 1005 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9880 1050 50  0001 C CNN
F 3 "~" H 9950 1050 50  0001 C CNN
	1    9950 1050
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0112
U 1 1 5C9AF569
P 9950 750
F 0 "#PWR0112" H 9950 600 50  0001 C CNN
F 1 "+3.3V" H 9965 923 50  0000 C CNN
F 2 "" H 9950 750 50  0001 C CNN
F 3 "" H 9950 750 50  0001 C CNN
	1    9950 750 
	1    0    0    -1  
$EndComp
Wire Wire Line
	9950 1200 10500 1200
Wire Wire Line
	10300 900  9950 900 
Wire Wire Line
	9950 900  9950 750 
Connection ~ 9950 900 
Wire Wire Line
	9950 1200 9950 1400
Connection ~ 9950 1200
$Comp
L power:GND #PWR0125
U 1 1 5C9C3011
P 9950 1900
F 0 "#PWR0125" H 9950 1650 50  0001 C CNN
F 1 "GND" H 9955 1727 50  0000 C CNN
F 2 "" H 9950 1900 50  0001 C CNN
F 3 "" H 9950 1900 50  0001 C CNN
	1    9950 1900
	1    0    0    -1  
$EndComp
$Comp
L NDC7001C:Q_DUAL_NMOS_PMOS_NDC7001C Q5
U 1 1 5C929CED
P 9850 1600
F 0 "Q5" H 10057 1646 50  0000 L CNN
F 1 "Q_DUAL_NMOS_PMOS_NDC7001C" H 10057 1555 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SuperSOT-6" H 10050 1600 50  0001 C CNN
F 3 "NDC7001C/2304413.pdf" H 10050 1600 50  0001 C CNN
	1    9850 1600
	1    0    0    -1  
$EndComp
$Comp
L NDC7001C:Q_DUAL_NMOS_PMOS_NDC7001C Q5
U 2 1 5C929DFE
P 10500 1000
F 0 "Q5" V 10845 1000 50  0000 C CNN
F 1 "Q_DUAL_NMOS_PMOS_NDC7001C" V 10754 1000 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SuperSOT-6" H 10700 1000 50  0001 C CNN
F 3 "NDC7001C/2304413.pdf" H 10700 1000 50  0001 C CNN
	2    10500 1000
	0    -1   -1   0   
$EndComp
Text Label 5550 5000 0    50   ~ 0
UART_RX_debugged
Text Label 5550 4900 0    50   ~ 0
UART_TX_debugged
Text Label 5750 3450 0    50   ~ 0
UART_TX_debugged
Wire Wire Line
	5750 3350 6450 3350
Text Label 5750 3350 0    50   ~ 0
UART_RX_debugged
Wire Wire Line
	6450 3450 5750 3450
Text Label 3550 2000 0    50   ~ 0
transistor_VDD
Wire Wire Line
	3550 2000 3550 2150
Connection ~ 3550 2150
Wire Wire Line
	3550 2150 3550 2250
Text Label 10700 900  0    50   ~ 0
transistor_VDD
Text Label 5900 2650 0    50   ~ 0
radioPowerSig
Text Label 9250 1600 0    50   ~ 0
radioPowerSig
Wire Wire Line
	6450 2650 5900 2650
Wire Wire Line
	2300 5700 2200 5700
Text Label 9750 6050 0    50   ~ 0
FT230_VCC
Text Label 4750 4450 0    50   ~ 0
FT230_VCC
Text Label 10250 5850 0    50   ~ 0
D+
Text Label 10250 5750 0    50   ~ 0
D-
Wire Wire Line
	10400 5750 10250 5750
Wire Wire Line
	10250 5850 10400 5850
$Comp
L Device:C C10
U 1 1 5C9775FA
P 4100 6050
F 0 "C10" H 4200 6050 50  0000 L CNN
F 1 "0.1uF" H 4200 5950 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4138 5900 50  0001 C CNN
F 3 "~" H 4100 6050 50  0001 C CNN
	1    4100 6050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0126
U 1 1 5C9776F0
P 4100 6200
F 0 "#PWR0126" H 4100 5950 50  0001 C CNN
F 1 "GND" H 4105 6027 50  0000 C CNN
F 2 "" H 4100 6200 50  0001 C CNN
F 3 "" H 4100 6200 50  0001 C CNN
	1    4100 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 6000 4750 6200
Wire Wire Line
	4750 6200 4100 6200
Wire Wire Line
	4750 6200 4950 6200
Wire Wire Line
	4950 6200 4950 6000
Connection ~ 4750 6200
Wire Wire Line
	4150 5500 4150 5750
Wire Wire Line
	4150 5900 4100 5900
Text Label 4950 4550 0    50   ~ 0
VCCIO
Wire Wire Line
	4950 4550 4950 4600
Text Label 3850 5750 0    50   ~ 0
VCCIO
Wire Wire Line
	3500 5200 3850 5200
Wire Wire Line
	3200 5300 3850 5300
Text Label 3850 5850 0    50   ~ 0
3V3OUT
Text Label 3850 4900 0    50   ~ 0
3V3OUT
Wire Wire Line
	4150 4900 3850 4900
Wire Wire Line
	3850 5850 4150 5850
Connection ~ 4150 5850
Wire Wire Line
	4150 5850 4150 5900
Wire Wire Line
	4750 4450 4750 4600
Wire Wire Line
	3850 5750 4150 5750
Wire Wire Line
	4150 5750 4150 5850
Connection ~ 4150 5750
Wire Wire Line
	2650 6700 2650 6600
Wire Wire Line
	2750 6700 2750 7150
Connection ~ 2750 7150
Wire Notes Line
	2600 6500 2600 7750
Wire Notes Line
	2600 7750 4400 7750
Wire Notes Line
	4400 7750 4400 6500
Wire Notes Line
	4400 6500 2600 6500
Connection ~ 4100 6200
Wire Wire Line
	5800 7450 5800 7500
Wire Notes Line
	6850 7750 6850 6700
Wire Notes Line
	4950 6700 4950 7750
Wire Notes Line
	4950 7750 6850 7750
Wire Notes Line
	4950 6700 6850 6700
Text Notes 5050 6650 0    50   ~ 0
Spenningsomformer 5V til 3.3V\n
Text Notes 2750 6450 0    50   ~ 0
Spenningsomformer Batteri til 5V\n
Text Notes 6350 2000 0    50   ~ 0
Microkontroller\n
Text Notes 8000 800  0    50   ~ 0
UART-tilkoblinger\nDebugging
Wire Notes Line
	9250 2150 11200 2150
Wire Notes Line
	11200 2150 11200 500 
Wire Notes Line
	11200 500  9250 500 
Wire Notes Line
	9250 500  9250 2150
Text Notes 10300 2050 0    50   ~ 0
Strømstyring radio
Text Notes 10200 4150 0    50   ~ 0
Sensortilkoblinger\n
Text Notes 5100 6000 0    50   ~ 0
Debugger\n
Wire Notes Line
	11150 5300 11150 6400
Wire Notes Line
	11150 6400 8700 6400
Wire Notes Line
	8700 6400 8700 5300
Wire Notes Line
	8700 5300 11150 5300
Text Notes 9000 5200 0    50   ~ 0
USB-micro\n
Wire Wire Line
	3450 2850 3400 2850
Wire Notes Line
	4100 4100 5400 4100
Wire Notes Line
	5400 4100 5400 2900
Wire Notes Line
	5400 2900 4100 2900
Wire Notes Line
	4100 2900 4100 4100
Text Notes 4200 2850 0    50   ~ 0
Stabiliserende kondensatorer\n
Wire Wire Line
	9950 1800 9950 1900
Wire Wire Line
	7150 1850 7250 1850
Connection ~ 7250 1850
$Comp
L power:GND #PWR0127
U 1 1 5CA54A8A
P 7250 5500
F 0 "#PWR0127" H 7250 5250 50  0001 C CNN
F 1 "GND" H 7255 5327 50  0000 C CNN
F 2 "" H 7250 5500 50  0001 C CNN
F 3 "" H 7250 5500 50  0001 C CNN
	1    7250 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 5500 7250 5250
Wire Wire Line
	7350 5250 7250 5250
Connection ~ 7250 5250
Wire Wire Line
	7250 5250 7150 5250
$Comp
L power:GND #PWR0128
U 1 1 5CA5E413
P 9400 2350
F 0 "#PWR0128" H 9400 2100 50  0001 C CNN
F 1 "GND" H 9405 2177 50  0000 C CNN
F 2 "" H 9400 2350 50  0001 C CNN
F 3 "" H 9400 2350 50  0001 C CNN
	1    9400 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9600 2500 9600 2350
Wire Wire Line
	9600 2350 9400 2350
Wire Wire Line
	9600 2500 10200 2500
$Comp
L power:+5V #PWR0129
U 1 1 5CA6A570
P 7250 1800
F 0 "#PWR0129" H 7250 1650 50  0001 C CNN
F 1 "+5V" H 7265 1973 50  0000 C CNN
F 2 "" H 7250 1800 50  0001 C CNN
F 3 "" H 7250 1800 50  0001 C CNN
	1    7250 1800
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:BSS138 Q7
U 1 1 5C92A95E
P 1600 1600
F 0 "Q7" V 1851 1600 50  0000 C CNN
F 1 "BSS138" V 1942 1600 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 1800 1525 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/BS/BSS138.pdf" H 1600 1600 50  0001 L CNN
	1    1600 1600
	0    1    1    0   
$EndComp
$Comp
L Transistor_FET:BSS138 Q6
U 1 1 5C95E69B
P 1600 1000
F 0 "Q6" V 1851 1000 50  0000 C CNN
F 1 "BSS138" V 1942 1000 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 1800 925 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/BS/BSS138.pdf" H 1600 1000 50  0001 L CNN
	1    1600 1000
	0    1    1    0   
$EndComp
Text Notes 1950 2000 0    50   ~ 0
Radio\n
Text Label 1400 2350 0    50   ~ 0
~ResetLora
Wire Wire Line
	1800 2350 1400 2350
Text Label 4400 1800 0    50   ~ 0
~ResetLora
Wire Wire Line
	4800 1800 4400 1800
Wire Wire Line
	1400 1700 1200 1700
$Comp
L Device:R R10
U 1 1 5C9C194A
P 1200 1550
F 0 "R10" H 1270 1596 50  0000 L CNN
F 1 "10K" H 1270 1505 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1130 1550 50  0001 C CNN
F 3 "~" H 1200 1550 50  0001 C CNN
	1    1200 1550
	1    0    0    -1  
$EndComp
Connection ~ 1200 1700
Wire Wire Line
	1200 1700 1000 1700
$Comp
L Device:R R12
U 1 1 5C9C2587
P 2000 1550
F 0 "R12" H 2070 1596 50  0000 L CNN
F 1 "10K" H 2070 1505 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1930 1550 50  0001 C CNN
F 3 "~" H 2000 1550 50  0001 C CNN
	1    2000 1550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R11
U 1 1 5C9C2AD9
P 2000 950
F 0 "R11" H 2070 996 50  0000 L CNN
F 1 "10K" H 2070 905 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1930 950 50  0001 C CNN
F 3 "~" H 2000 950 50  0001 C CNN
	1    2000 950 
	1    0    0    -1  
$EndComp
$Comp
L Device:R R9
U 1 1 5C9C3214
P 1200 950
F 0 "R9" H 1270 996 50  0000 L CNN
F 1 "10K" H 1270 905 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1130 950 50  0001 C CNN
F 3 "~" H 1200 950 50  0001 C CNN
	1    1200 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 1400 1200 1400
Connection ~ 1200 800 
Wire Wire Line
	1200 800  1600 800 
Wire Wire Line
	1400 1100 1200 1100
Connection ~ 1200 1100
Wire Wire Line
	1800 1100 2000 1100
Connection ~ 2000 1100
Wire Wire Line
	2000 800  2250 800 
Wire Wire Line
	1800 1700 2000 1700
Connection ~ 2000 1700
Wire Wire Line
	2000 1700 2350 1700
Wire Wire Line
	2000 1400 2250 1400
$Comp
L power:+3V3 #PWR0130
U 1 1 5C9EA6C6
P 1200 800
F 0 "#PWR0130" H 1200 650 50  0001 C CNN
F 1 "+3V3" H 1215 973 50  0000 C CNN
F 2 "" H 1200 800 50  0001 C CNN
F 3 "" H 1200 800 50  0001 C CNN
	1    1200 800 
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0131
U 1 1 5C9FCD62
P 2250 1400
F 0 "#PWR0131" H 2250 1250 50  0001 C CNN
F 1 "+5V" H 2265 1573 50  0000 C CNN
F 2 "" H 2250 1400 50  0001 C CNN
F 3 "" H 2250 1400 50  0001 C CNN
	1    2250 1400
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0132
U 1 1 5C9FE5E5
P 2250 800
F 0 "#PWR0132" H 2250 650 50  0001 C CNN
F 1 "+5V" H 2265 973 50  0000 C CNN
F 2 "" H 2250 800 50  0001 C CNN
F 3 "" H 2250 800 50  0001 C CNN
	1    2250 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 1100 2350 1100
Text Label 2350 1100 0    50   ~ 0
lora_tx_5V
Text Label 2350 1700 0    50   ~ 0
lora_rx_5V
Text Label 5900 2450 0    50   ~ 0
lora_rx_5V
Text Label 5900 2550 0    50   ~ 0
lora_tx_5V
$Comp
L power:+3V3 #PWR0133
U 1 1 5CA19C63
P 1200 1400
F 0 "#PWR0133" H 1200 1250 50  0001 C CNN
F 1 "+3V3" H 1215 1573 50  0000 C CNN
F 2 "" H 1200 1400 50  0001 C CNN
F 3 "" H 1200 1400 50  0001 C CNN
	1    1200 1400
	1    0    0    -1  
$EndComp
Connection ~ 1200 1400
Text Label 1000 1700 2    50   ~ 0
lora_rx
Text Label 1000 1100 2    50   ~ 0
lora_tx
Wire Wire Line
	1000 1100 1200 1100
Text Label 1700 2850 2    50   ~ 0
lora_rx
Text Label 3450 2850 0    50   ~ 0
lora_tx
Text Label 8500 1200 0    50   ~ 0
lora_tx
Text Label 8500 1100 0    50   ~ 0
lora_rx
Wire Wire Line
	8500 1100 8200 1100
Wire Wire Line
	8200 1200 8500 1200
Text Label 2300 5700 0    50   ~ 0
UPDI
Wire Wire Line
	9250 1600 9650 1600
$Comp
L power:+5V #PWR0134
U 1 1 5CA7408A
P 4500 3250
F 0 "#PWR0134" H 4500 3100 50  0001 C CNN
F 1 "+5V" H 4515 3423 50  0000 C CNN
F 2 "" H 4500 3250 50  0001 C CNN
F 3 "" H 4500 3250 50  0001 C CNN
	1    4500 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 3250 4500 3350
$Comp
L Regulator_Linear:MCP1703A-3302_SOT223 U2
U 1 1 5CBDCDE8
P 5800 7050
F 0 "U2" H 5800 7292 50  0000 C CNN
F 1 "MCP1703A-3302_SOT223" H 5800 7201 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 5800 7250 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20005122B.pdf" H 5800 7000 50  0001 C CNN
	1    5800 7050
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:MCP1703A-5002_SOT223 U1
U 1 1 5CBDE445
P 3400 7150
F 0 "U1" H 3400 7392 50  0000 C CNN
F 1 "MCP1703A-5002_SOT223" H 3400 7301 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 3400 7350 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20005122B.pdf" H 3400 7100 50  0001 C CNN
	1    3400 7150
	1    0    0    -1  
$EndComp
$EndSCHEMATC
