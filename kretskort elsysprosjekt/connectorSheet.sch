EESchema Schematic File Version 4
LIBS:kretskort elsysprosjekt-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 1250 1200 0    50   Input ~ 0
5V
Text HLabel 1250 1700 0    50   Input ~ 0
GND
Text HLabel 1100 2300 0    50   Input ~ 0
pH-sig
$Comp
L Connector:Conn_01x03_Female J6
U 1 1 5C669FDE
P 6100 3850
F 0 "J6" H 6127 3876 50  0000 L CNN
F 1 "pH sensor" H 6127 3785 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 6100 3850 50  0001 C CNN
F 3 "~" H 6100 3850 50  0001 C CNN
	1    6100 3850
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Female J5
U 1 1 5C66A042
P 5950 2600
F 0 "J5" H 5977 2626 50  0000 L CNN
F 1 "turb sensor" H 5977 2535 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 5950 2600 50  0001 C CNN
F 3 "~" H 5950 2600 50  0001 C CNN
	1    5950 2600
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Female J7
U 1 1 5C66A07C
P 6100 5150
F 0 "J7" H 6127 5176 50  0000 L CNN
F 1 "condctivity sensor" H 6127 5085 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 6100 5150 50  0001 C CNN
F 3 "~" H 6100 5150 50  0001 C CNN
	1    6100 5150
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Female J4
U 1 1 5C66A0BC
P 5950 1000
F 0 "J4" H 5977 1026 50  0000 L CNN
F 1 "temp sensor" H 5977 935 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 5950 1000 50  0001 C CNN
F 3 "~" H 5950 1000 50  0001 C CNN
	1    5950 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 2600 5750 2600
Text HLabel 1250 1900 0    50   Input ~ 0
tempSig
Text HLabel 1200 2100 0    50   Input ~ 0
turbSig
Text HLabel 1200 2500 0    50   Input ~ 0
kondSig
Wire Wire Line
	1250 1900 4050 1900
Wire Wire Line
	4050 1900 4050 900 
Wire Wire Line
	4050 900  5750 900 
Wire Wire Line
	1200 2100 4350 2100
Wire Wire Line
	4350 2500 5750 2500
Wire Wire Line
	1200 2500 2450 2500
Text Label 6100 950  0    50   ~ 0
tempConnector
Text Label 6100 2550 0    50   ~ 0
turbConnector
Text Label 6200 3700 0    50   ~ 0
pHconnector
Text Label 6250 5100 0    50   ~ 0
konduktivitetConnector
Text HLabel 1250 1400 0    50   Input ~ 0
3V3
Wire Wire Line
	3000 1200 1250 1200
Wire Wire Line
	1250 1400 4450 1400
Wire Wire Line
	4450 1400 4450 1000
Wire Wire Line
	4450 1000 5750 1000
$Comp
L Transistor_FET:MMBF170 Q1
U 1 1 5C6E8729
P 5650 1300
F 0 "Q1" H 5855 1346 50  0000 L CNN
F 1 "MMBF170" H 5855 1255 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5850 1225 50  0001 L CIN
F 3 "https://www.diodes.com/assets/Datasheets/ds30104.pdf" H 5650 1300 50  0001 L CNN
	1    5650 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 1500 7650 1500
Wire Wire Line
	7650 1500 7650 1650
$Comp
L power:GND #PWR0114
U 1 1 5C6EDA4C
P 7650 1650
F 0 "#PWR0114" H 7650 1400 50  0001 C CNN
F 1 "GND" H 7655 1477 50  0000 C CNN
F 2 "" H 7650 1650 50  0001 C CNN
F 3 "" H 7650 1650 50  0001 C CNN
	1    7650 1650
	1    0    0    -1  
$EndComp
Connection ~ 3000 2600
Wire Wire Line
	3000 1200 3000 2600
Wire Wire Line
	2450 5050 5900 5050
Wire Wire Line
	2450 2500 2450 5050
Wire Wire Line
	1100 2300 2700 2300
Wire Wire Line
	2700 2300 2700 3750
Wire Wire Line
	2700 3750 5900 3750
Wire Wire Line
	3000 3850 5900 3850
Wire Wire Line
	3000 2600 3000 3850
Wire Wire Line
	3000 3850 3000 5150
Wire Wire Line
	3000 5150 5900 5150
Connection ~ 3000 3850
Wire Wire Line
	4350 2100 4350 2500
Text HLabel 1100 2800 0    50   Input ~ 0
tempSwitch
Text HLabel 1100 3000 0    50   Input ~ 0
turbSwitch
Text HLabel 1100 3200 0    50   Input ~ 0
pHSwitch
Text HLabel 1100 3350 0    50   Input ~ 0
kondSwitch
Wire Wire Line
	5450 1300 5050 1300
Wire Wire Line
	1650 1300 1650 2800
Wire Wire Line
	1650 2800 1100 2800
Wire Wire Line
	5050 1300 5050 1500
Connection ~ 5050 1300
Wire Wire Line
	5050 1300 1650 1300
$Comp
L Device:R R2
U 1 1 5C6F560B
P 5050 1650
F 0 "R2" H 5120 1696 50  0000 L CNN
F 1 "10k" H 5120 1605 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4980 1650 50  0001 C CNN
F 3 "~" H 5050 1650 50  0001 C CNN
	1    5050 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 1800 5050 1800
$Comp
L power:GND #PWR0115
U 1 1 5C6F5CDC
P 5150 1800
F 0 "#PWR0115" H 5150 1550 50  0001 C CNN
F 1 "GND" H 5155 1627 50  0000 C CNN
F 2 "" H 5150 1800 50  0001 C CNN
F 3 "" H 5150 1800 50  0001 C CNN
	1    5150 1800
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:MMBF170 Q2
U 1 1 5C6F685B
P 5650 2900
F 0 "Q2" H 5855 2946 50  0000 L CNN
F 1 "MMBF170" H 5855 2855 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5850 2825 50  0001 L CIN
F 3 "https://www.diodes.com/assets/Datasheets/ds30104.pdf" H 5650 2900 50  0001 L CNN
	1    5650 2900
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:MMBF170 Q3
U 1 1 5C6F6A25
P 5800 4150
F 0 "Q3" H 6005 4196 50  0000 L CNN
F 1 "MMBF170" H 6005 4105 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6000 4075 50  0001 L CIN
F 3 "https://www.diodes.com/assets/Datasheets/ds30104.pdf" H 5800 4150 50  0001 L CNN
	1    5800 4150
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:MMBF170 Q4
U 1 1 5C6F6B39
P 5800 5450
F 0 "Q4" H 6005 5496 50  0000 L CNN
F 1 "MMBF170" H 6005 5405 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6000 5375 50  0001 L CIN
F 3 "https://www.diodes.com/assets/Datasheets/ds30104.pdf" H 5800 5450 50  0001 L CNN
	1    5800 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 2900 5100 2900
Wire Wire Line
	2000 2900 2000 3000
Wire Wire Line
	2000 3000 1100 3000
Wire Wire Line
	1100 3200 4100 3200
Wire Wire Line
	4100 3200 4100 4150
Wire Wire Line
	4100 4150 5100 4150
Wire Wire Line
	1100 3350 1600 3350
Wire Wire Line
	1600 3350 1600 5450
Wire Wire Line
	1600 5450 5150 5450
$Comp
L Device:R R3
U 1 1 5C6FBB70
P 5100 3050
F 0 "R3" H 5170 3096 50  0000 L CNN
F 1 "10k" H 5170 3005 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5030 3050 50  0001 C CNN
F 3 "~" H 5100 3050 50  0001 C CNN
	1    5100 3050
	1    0    0    -1  
$EndComp
Connection ~ 5100 2900
Wire Wire Line
	5100 2900 2000 2900
$Comp
L Device:R R4
U 1 1 5C6FBCFD
P 5100 4300
F 0 "R4" H 5170 4346 50  0000 L CNN
F 1 "10k" H 5170 4255 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5030 4300 50  0001 C CNN
F 3 "~" H 5100 4300 50  0001 C CNN
	1    5100 4300
	1    0    0    -1  
$EndComp
Connection ~ 5100 4150
Wire Wire Line
	5100 4150 5600 4150
$Comp
L Device:R R5
U 1 1 5C6FDD0E
P 5150 5600
F 0 "R5" H 5220 5646 50  0000 L CNN
F 1 "10k" H 5220 5555 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5080 5600 50  0001 C CNN
F 3 "~" H 5150 5600 50  0001 C CNN
	1    5150 5600
	1    0    0    -1  
$EndComp
Connection ~ 5150 5450
Wire Wire Line
	5150 5450 5600 5450
$Comp
L power:GND #PWR0116
U 1 1 5C6FF27F
P 5100 3350
F 0 "#PWR0116" H 5100 3100 50  0001 C CNN
F 1 "GND" H 5105 3177 50  0000 C CNN
F 2 "" H 5100 3350 50  0001 C CNN
F 3 "" H 5100 3350 50  0001 C CNN
	1    5100 3350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0117
U 1 1 5C6FF43D
P 5100 4550
F 0 "#PWR0117" H 5100 4300 50  0001 C CNN
F 1 "GND" H 5105 4377 50  0000 C CNN
F 2 "" H 5100 4550 50  0001 C CNN
F 3 "" H 5100 4550 50  0001 C CNN
	1    5100 4550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0118
U 1 1 5C6FFA7A
P 5150 5900
F 0 "#PWR0118" H 5150 5650 50  0001 C CNN
F 1 "GND" H 5155 5727 50  0000 C CNN
F 2 "" H 5150 5900 50  0001 C CNN
F 3 "" H 5150 5900 50  0001 C CNN
	1    5150 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 5900 5150 5750
Wire Wire Line
	5100 4550 5100 4450
Wire Wire Line
	5100 3350 5100 3200
$Comp
L power:GND #PWR0119
U 1 1 5C7021F4
P 7700 3200
F 0 "#PWR0119" H 7700 2950 50  0001 C CNN
F 1 "GND" H 7705 3027 50  0000 C CNN
F 2 "" H 7700 3200 50  0001 C CNN
F 3 "" H 7700 3200 50  0001 C CNN
	1    7700 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0120
U 1 1 5C70231C
P 7700 4400
F 0 "#PWR0120" H 7700 4150 50  0001 C CNN
F 1 "GND" H 7705 4227 50  0000 C CNN
F 2 "" H 7700 4400 50  0001 C CNN
F 3 "" H 7700 4400 50  0001 C CNN
	1    7700 4400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0121
U 1 1 5C702417
P 7650 5800
F 0 "#PWR0121" H 7650 5550 50  0001 C CNN
F 1 "GND" H 7655 5627 50  0000 C CNN
F 2 "" H 7650 5800 50  0001 C CNN
F 3 "" H 7650 5800 50  0001 C CNN
	1    7650 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 5650 7650 5650
Wire Wire Line
	7650 5650 7650 5800
Wire Wire Line
	7700 4400 5900 4400
Wire Wire Line
	5900 4400 5900 4350
Wire Wire Line
	5750 3100 7700 3100
Wire Wire Line
	7700 3100 7700 3200
$EndSCHEMATC
